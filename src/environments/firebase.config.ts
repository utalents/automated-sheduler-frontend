import {AuthMethods, AuthProviders} from "angularfire2";

export const firebaseConfig = {
  apiKey: "AIzaSyCi76zeTZgyBeOgJ8gD5LxXrx6CwrXQz0Y",
  authDomain: "utalents-automation.firebaseapp.com",
  databaseURL: "https://utalents-automation.firebaseio.com",
  projectId: "utalents-automation",
  storageBucket: "utalents-automation.appspot.com",
  messagingSenderId: "304613691587"
};

export const authConfig = {
    provider: AuthProviders.Password,
    method: AuthMethods.Password
};
