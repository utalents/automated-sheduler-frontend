import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs/Rx";
import { ProjectService } from "app/shared/services/project.service";
import { Project } from "../../shared/model/project";
import * as _ from "lodash";

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css'],
  providers: [ProjectService]
})
export class ProjectsComponent implements OnInit {
  
  projects$: Observable<Project[]>;
  projects: Project[];
  selectedProject: Project;
  states : [{$key: string, value: string}];
  opened: boolean = false;
  deleteOpened: boolean = false;

  constructor(private projectService: ProjectService) { }

  ngOnInit() {
    this.projects$ = this.projectService.findAllProjects();
    this.projects$.subscribe(projects => {
      this.projects = projects;
      this.projects.forEach( project =>{
        this.projectService.findProjectsQuequeCount(project.$key)
          .subscribe( items => project.quequeCount = Object.keys(items).length);

        this.projectService.findProjectsExpertsCount(project.$key)
          .subscribe( items => project.expertCount = Object.keys(items).length);
      })
    })
    this.selectedProject = Project.empty();
    //TODO change to state machine.
    this.states = [
      {$key:'1', value:"Активен"}, 
      {$key:'2', value:"Остановлен"}
    ];
  }

  onCreate($event){
    $event.preventDefault();
    this.opened = true;
  }

  onEdit(project: Project){
    this.selectedProject = project;
    this.opened = true;
  }

  onDelete(project: Project){
    this.selectedProject = project;
    this.deleteOpened = true;
  }
  cancelDeletion(){
    this.deleteOpened = false;
  }
  deleteProject(){
    this.projectService.disableProject(this.selectedProject.$key, this.selectedProject);
    this.cancelDeletion();
  }

  getStateById($key: string): any
  {
    return this.getById(this.states, $key);
  }
  
  private getById(data: any[], $key: string): any {
    let result = _.find( data, (item: any) => item.$key == $key);
    return result;
  }

  canDelete(project: Project):boolean{
    return project.quequeCount == 0 && project.expertCount == 0;
  }
  
  onNotify(message:string):void {
    console.log(message);
    if (message == "clearData"){
      this.selectedProject = Project.empty();
      this.selectedProject.stateId = undefined;
      this.opened = false;
    }
  }

}
