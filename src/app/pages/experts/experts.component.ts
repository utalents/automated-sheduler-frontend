import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs/Rx";

import {ExpertsService} from "app/shared/services/experts.service";
import {Expert} from "app/shared/model/expert";

@Component({
  selector: 'app-experts',
  templateUrl: './experts.component.html',
  styleUrls: ['./experts.component.css'],
  providers: [ExpertsService],
})
export class ExpertsComponent implements OnInit {

  experts$: Observable<Expert[]>;
  experts: Expert[];
  selectedExpert: Expert;
  private projectId: string;
  opened: boolean = false;
  deleteOpened: boolean = false;

  constructor(private expertsService: ExpertsService) { }

  ngOnInit() {
    this.projectId == undefined;
    this.selectedExpert = Expert.empty();
  }

  onCreate($event){
    $event.preventDefault();
    this.opened = true;
  }
  onEdit(expert: Expert){
    this.selectedExpert = expert;
    this.opened = true;
  } 
  onDelete(expert: Expert){
    this.selectedExpert = expert;
    this.deleteOpened = true;
  }

  cancelDeletion(){
    this.deleteOpened = false;
  }

  deleteExpert(){
    this.selectedExpert.description = this.selectedExpert.description || null;
    this.expertsService.disableExpert(this.selectedExpert.$key, this.selectedExpert);
    this.cancelDeletion();
  }

  onNotify(message:string):void {
    console.log(message);
    if (message == "clearData"){
      this.selectedExpert = Expert.empty();
      this.selectedExpert.projectId = this.projectId;
      this.opened = false;
    }
  }

  canDelete(expert: Expert): boolean{
    return expert.quequeCount == 0;
  }
  onProjectSelected(projectId: string):void{
    console.log("project selected");
    this.projectId = projectId;
    this.experts$ = this.expertsService.findExpertByProject(this.projectId);
    this.experts$.subscribe( experts => {
      this.experts = experts;
      this.experts.forEach( expert => {
        this.expertsService.findExpertsQuequeCount(expert.$key)
          .subscribe( items => expert.quequeCount = Object.keys(items).length);
      })
    });
  }


}