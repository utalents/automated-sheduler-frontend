import { Component, OnInit } from '@angular/core';
import {Validators, FormGroup, FormBuilder} from "@angular/forms";
import {AuthService} from "../../shared/security/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    loginForm:FormGroup;
    loginFailed: boolean = false;

    constructor(private fb:FormBuilder, private authService: AuthService,
                private router:Router) {

        this.loginForm = this.fb.group({
            email: ['',Validators.required],
            password: ['',Validators.required]
        });
    }

    ngOnInit() {
        this.authService.authInfo$
        .subscribe(authInfo =>  {
            if (authInfo.isLoggedIn()) this.router.navigate(['/sandbox'])
        });
    }

    login() {
        const formValue = this.loginForm.value;
        this.authService.login(formValue.email, formValue.password)
            .subscribe(
                () => {
                    this.loginFailed = false;
                    this.router.navigate(['/sandbox'])
                },
                (message: string) => {
                    this.loginFailed = true;
                    console.log(message)
                }
            );
    }

}
