import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs/Rx";

import * as _ from "lodash";

import {MailService} from "app/shared/services/mail.service";
import {MailTask} from "app/shared/model/mailTask";

import {ExpertsService} from "app/shared/services/experts.service";
import { Expert } from "app/shared/model/expert";
import { MailQuequeState } from "app/shared/model/mailQuequeState";
import { MailQuequeStateService } from "app/shared/services/mail-queque-state.service";


@Component({
  selector: 'app-mail-queque',
  templateUrl: './mail-queque.component.html',
  styleUrls: ['./mail-queque.component.css'],
  providers: [MailService, ExpertsService, MailQuequeStateService]
})
export class MailQuequeComponent implements OnInit {

  queques$: Observable<MailTask[]>;
  queques: MailTask[];
  experts: Expert[];
  projectId: string;
  states: MailQuequeState[];
  selectedQueque : MailTask;
  opened: boolean = false;
  deleteOpened: boolean = false;


  constructor(private mailService: MailService, 
              private expertsService: ExpertsService,
              private mailQuequeStateService: MailQuequeStateService
              ){ 
  }

  ngOnInit() {
      this.projectId = undefined;
      this.selectedQueque = MailTask.empty();
  }

  onCreate($event){
    $event.preventDefault();
    this.opened = true;
  }
  onEdit(queque: MailTask){
    this.selectedQueque = queque;
    this.opened = true;
  } 
  onDelete(queque: MailTask){
    this.selectedQueque = queque;
    this.deleteOpened = true;
  }
  cancelDeletion(){
    this.deleteOpened = false;
  }
  deleteQueque(){
    this.selectedQueque.documentId = this.selectedQueque.documentId || null;
    this.mailService.disableQueque(this.selectedQueque.$key, this.selectedQueque);
    this.cancelDeletion();
  }

  getExpertById($key: string): any
  {
    return this.getById(this.experts, $key);
  }

  getStateById($key: string):any {
    return this.getById(this.states, $key);
  }

  // TODO create parent class and extends it of him
  onNotify(message:string):void {
    console.log(message);
    if (message == "clearData"){
      this.selectedQueque = MailTask.empty();
      this.selectedQueque.stateId = this.states[0].$key;
      this.selectedQueque.expertId = undefined;
      this.opened = false;
    }
  }
  // TODO create parent class and extends it of him
  private getById(data: any[], $key: string): any {
    let result = _.find( data, (item: any) => item.$key == $key);
    return result;
  }

  canEdit(queque: MailTask):boolean{
    return this.getStateById(queque.stateId).state != "Canceled";
  }
  canDelete(queque):boolean{
    return this.getStateById(queque.stateId).state == "Created";
  }

  onProjectSelected(projectId: string):void{
    console.log("project selected");
    this.projectId = projectId;
    this.queques$ = this.mailService.findQuequesByProject(this.projectId);
    this.queques$.subscribe( queques => this.queques = queques);
    this.expertsService.findExpertByProject(this.projectId).subscribe(
        experts => this.experts = experts
    );
    this.mailQuequeStateService.findAllStates().subscribe(
        states => this.states = states
    );
  }

}
