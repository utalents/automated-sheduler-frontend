import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MailQuequeComponent } from './mail-queque.component';

describe('MailQuequeComponent', () => {
  let component: MailQuequeComponent;
  let fixture: ComponentFixture<MailQuequeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MailQuequeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailQuequeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
