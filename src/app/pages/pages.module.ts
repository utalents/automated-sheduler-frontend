import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ClarityModule } from 'clarity-angular';
//DEPRICATED
import { ButtonModule }  from 'primeng/primeng';

import { ExpertsComponent } from "./experts/experts.component";
import { LoginComponent } from "./login/login.component";
import { MailQuequeComponent } from "./mail-queque/mail-queque.component";
import { ProjectsComponent } from "./projects/projects.component";
import { SandboxComponent } from "./sandbox/sandbox.component";
import { ComponentsModule } from "app/components/components.module";

const components = [
  ExpertsComponent,
  LoginComponent,
  MailQuequeComponent,
  ProjectsComponent,
  SandboxComponent,
];

@NgModule({
  imports: [
    ClarityModule,
    CommonModule,
    ComponentsModule,
    ButtonModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
  ],
  declarations: [
    ...components,
  ],
  exports: [
    ...components,
  ]
})
export class PagesModule { }

