import { NgModule } from '@angular/core';
import {Route, RouterModule } from "@angular/router";
import {AuthGuard} from "./shared/security/auth.guard";

import { ExpertsComponent} from "./pages/experts/experts.component";
import { LoginComponent} from "./pages/login/login.component";
import { MailQuequeComponent } from "./pages/mail-queque/mail-queque.component";
import { ProjectsComponent } from "./pages/projects/projects.component";
import { SandboxComponent} from "./pages/sandbox/sandbox.component";

export const routerConfig : Route[] = 
    [
        { path: '', redirectTo: 'login', pathMatch: 'full' },
        { path:'sandbox', component: SandboxComponent },
        { path: 'login', component: LoginComponent },
        {
            path: 'experts',
            children: [
                {
                    path: '',
                    component: ExpertsComponent,
                    canActivate: [AuthGuard]
                }
            ]
        },
        {
            path: 'mail-queque',
            children: [
                {
                    path: '',
                    component: MailQuequeComponent,
                    canActivate: [AuthGuard]
                }
            ]
        },
        {
            path: 'projects',
            children: [
                {
                    path: '',
                    component: ProjectsComponent,
                    canActivate: [AuthGuard]
                }
            ]
        },
        { path: '**', redirectTo: 'login' },
    ];

@NgModule({
  imports: [
    RouterModule.forRoot(routerConfig),
  ],
  exports: [
    RouterModule,
  ],
})
export class AppRoutingModule { }