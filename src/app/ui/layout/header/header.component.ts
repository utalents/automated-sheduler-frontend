import { Component, OnInit } from '@angular/core';
import { AuthService } from "app/shared/security/auth.service";
import { AuthInfo } from "app/shared/security/auth-info";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  headerLinks:  [ {link: string, icon: string }];
  _subLinks:     [ {link: string, label: string, secured: boolean}];

  authInfo: AuthInfo;

  constructor(private authService:AuthService) { 
  }

  ngOnInit() {
    this.authService.authInfo$.subscribe(authInfo =>  this.authInfo = authInfo);
    this.headerLinks = [
      { link: 'dashboard', icon: 'home'},
      { link: 'settings', icon: 'cog'},
    ];
     this._subLinks = [
      { link : 'sandbox', label: 'Sandbox', secured: false},
      { link : 'projects', label: 'Проекты', secured: true},
      { link : 'experts', label: 'Эксперты', secured: true},
      { link : 'mail-queque', label: 'Отправка писем', secured: true}
     ]
  }

  getSublinks(){
    if (this.authInfo.isLoggedIn())
    {
      return this._subLinks;
    }
    else
    {
      return this._subLinks.filter(links => !links.secured);
    }
  }

  logout() {
        this.authService.logout();
  }

}
