import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ClarityModule} from "clarity-angular";

import { AppComponent } from './app.component';
import { UiModule } from './ui/ui.module';
import { AppRoutingModule } from "app/app.routing";
import { PagesModule } from "app/pages/pages.module";

import {firebaseConfig, authConfig} from "../environments/firebase.config";
import {AngularFireModule} from "angularfire2/index";

import {AuthService} from "./shared/security/auth.service";
import {AuthGuard} from "./shared/security/auth.guard";

import { SafeUrlPipe } from './shared/security/safe-url.pipe';

//TODO read about rxjs
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/switchMap';

@NgModule({
  declarations: [
    AppComponent,
    SafeUrlPipe,
  ],
  imports: [
    AngularFireModule.initializeApp(firebaseConfig, authConfig),
    AppRoutingModule,
    BrowserModule,
    ClarityModule.forRoot(),
    FormsModule,
    HttpModule,
    PagesModule,
    UiModule,
  ],
  providers: [AuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
