import { Injectable } from '@angular/core';
import {Observable, Subject, BehaviorSubject} from "rxjs/Rx";
import {AngularFireAuth, FirebaseAuthState} from "angularfire2";
import {AuthInfo} from "./auth-info";
import {Router} from "@angular/router";

@Injectable()
export class AuthService {

  static UNKNOWN_USER = AuthInfo.empty();
  authInfo$: BehaviorSubject<AuthInfo> = new BehaviorSubject<AuthInfo>(AuthService.UNKNOWN_USER);

  constructor(private auth: AngularFireAuth, private router:Router) {
        let localStorageValue = localStorage.getItem('authInfo');
        if (localStorageValue){
            let parsedValue = <AuthInfo>JSON.parse(localStorageValue);
            const authInfo = new AuthInfo(
                parsedValue.$uid, 
                parsedValue.userName,
                parsedValue.timestamp
            );
            this.authInfo$.next(authInfo);
        }
  }
    login(email, password):Observable<FirebaseAuthState> {
        let authState = this.fromFirebaseAuthPromise(this.auth.login({email, password}));
        return authState;
    }

    signUp(email, password) {
        return this.fromFirebaseAuthPromise(this.auth.createUser({email, password}));
    }

    logout() {
        this.auth.logout();
        this.authInfo$.next(AuthService.UNKNOWN_USER);
        localStorage.removeItem("authInfo");
        this.router.navigate(['/login']);
    }

    /*
     *
     * This is a demo on how we can 'Observify' any asynchronous interaction
     *
     *
     * */
    fromFirebaseAuthPromise(promise):Observable<any> {
        const subject = new Subject<any>();
        promise
            .then(res => {
                    const authInfo = new AuthInfo(
                        this.auth.getAuth().uid, 
                        this.auth.getAuth().auth.email,
                        (+new Date()).toString()
                    );
                    localStorage.setItem("authInfo", JSON.stringify(authInfo));
                    this.authInfo$.next(authInfo);
                    subject.next(res);
                    subject.complete();
                },
                err => {
                    // this breakes auth info class.
                    //this.authInfo$.error(err);
                    subject.error(err);
                    subject.complete();
                });
        return subject.asObservable();
    }
}
