export class AuthInfo {
    constructor(
        public $uid:string,
        public userName: string,
        public timestamp: string
    ) {

    }
    
    static empty(){
        return new AuthInfo(null, null, null);
    }

    isLoggedIn(): boolean {
        return !!this.$uid
    }

}

        
