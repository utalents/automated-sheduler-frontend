import {Observable} from "rxjs/Rx";

export class Expert {

    constructor(
      public $key:string,
      public email:string,
      public name: string,
      public surname: string,
      public patronymic: string,
      public description: string,
      public projectId: string,
      public creation: Date,
      public edition: Date,
      public enabled: boolean
    ){

    }

    public quequeCount: number = 0;

    get fullName():string {
      return [this.surname, this.name, this.patronymic].join(" "); 
    }

    static empty(){
      return new Expert(undefined, '', '', '', '', '', '', new Date(), null, true);
    }

    static fromJson({$key, email, name, surname, patronymic, description, projectId, creation, edition, enabled }){
      return new Expert($key, email, name, surname, patronymic, description, projectId, creation, edition, enabled);
    }

    static fromJsonArray(json : any[]) : Expert[] {
      return json.map(Expert.fromJson);
    }

}