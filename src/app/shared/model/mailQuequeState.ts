export class MailQuequeState {
    
  constructor(
    public $key: string,
    public state: string,
    public value: string
  ){

  }

  static empty(){
    return new MailQuequeState(undefined, '', '');
  }

  static fromJson({$key, state, value }){
    return new MailQuequeState($key, state, value );
  }

  static fromJsonArray(json : any[]) : MailQuequeState[] {
    return json.map(MailQuequeState.fromJson);
  }

}