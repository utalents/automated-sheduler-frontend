export class Project {
    
    constructor(
        public $key: string,
        public name: string,
        public appScriptId: string,
        public stateId: string, //TODO check state machine for angular2
        public creation: Date,
        public edition: Date,
        public enabled: boolean
    ){

    }


    public quequeCount: number = 0;
    public expertCount: number = 0;

    static empty(){
        return new Project(undefined, '', '', '', new Date(), null, true);
    }

    static fromJson({$key, name, appScriptId, stateId, creation, edition, enabled}){
        return new Project($key, name, appScriptId, stateId, creation, edition, enabled);
    }

    static fromJsonArray(json : any[]) : Project[] {
        return json.map(Project.fromJson);
    }
}
