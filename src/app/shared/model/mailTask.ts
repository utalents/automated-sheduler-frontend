export class MailTask {
  constructor(
    public $key: string,
    public requestId: string,
    public expertId: string,
    public stateId: string, //TODO check state machine for angular2
    public documentId: string,
    public projectId: string,
    public sendDate: Date,
    public generateDate: Date,
    public creation: Date,
    public edition: Date,
    public enabled: boolean
  ){

  }

  static empty(){
    return new MailTask(undefined, '', '', '', '', '', null, null, new Date(), null, true);
  }

  static fromJson({$key, requestId, expertId, stateId, documentId, projectId, creation, sendDate, generateDate, edition, enabled }){
    return new MailTask($key, requestId, expertId, stateId, documentId, projectId, sendDate, generateDate, creation, edition, enabled);
  }

  static fromJsonArray(json : any[]) : MailTask[] {
    return json.map(MailTask.fromJson);
  }

}
