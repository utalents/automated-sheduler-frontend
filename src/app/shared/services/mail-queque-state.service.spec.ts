import { TestBed, inject } from '@angular/core/testing';

import { MailQuequeStateService } from './mail-queque-state.service';

describe('MailQuequeStateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MailQuequeStateService]
    });
  });

  it('should ...', inject([MailQuequeStateService], (service: MailQuequeStateService) => {
    expect(service).toBeTruthy();
  }));
});
