import { Injectable, Inject} from '@angular/core';
import {Http} from "@angular/http";

import {AngularFireDatabase, FirebaseRef} from "angularfire2";
import {FirebaseListFactoryOpts} from "angularfire2/interfaces";
import {firebaseConfig} from "../../../environments/firebase.config";

import {Observable, Subject} from "rxjs/Rx";

import {DataService} from "./data.service";

import {Expert} from "../model/expert";

@Injectable()
export class ExpertsService extends DataService {
    
  constructor(private db:AngularFireDatabase, @Inject(FirebaseRef) fb, private http:Http) {
    super(db, fb, http);
  }

  findAllExperts():Observable<Expert[]>{
      return this.db.list('experts', {
        query: {
          orderByChild: 'enabled',
          equalTo: true
        }
      }).map(Expert.fromJsonArray);
  }

  findExpertByEmail(expertEmail: string):Observable<Expert>{
      return this.db.list('experts', {
        query: {
          orderByChild: 'email',
          equalTo: expertEmail
        }
      }).map(results => results[0]);
  }

  findExpertByProject(projectId: string): Observable<Expert[]>{
    return this.db.list('experts',{
      query: {
        orderByChild: 'projectId',
        equalTo: projectId,
      }
    })
    .map(experts => experts.filter(expert => expert.enabled))
    .map(Expert.fromJsonArray);
  }

  findExpertById(expertId: string): Observable<Expert>{
    return this.db.list('experts',{
      query: {
        orderByKey: true,
        equalTo: expertId
      }
    }).map(results => results[0]);
  }

  findExpertsQuequeCount(expertid: string): Observable<any>{
    return this.db.list('mailQuequesPerExpert/'+ expertid,{
      query: {
        orderByValue: true,
        equalTo: true
      }
    })
  }

  createNewExpert(projectId: string, expert: any): Observable<any> {

    const expertToSave = Object.assign({}, expert, {projectId}, {enabled: true, creation: new Date(), edition: null});
    const newExpertKey = this.sdkDb.child('experts').push().key;
    let dataToSave = {};

    dataToSave["experts/" + newExpertKey] = expertToSave;
    dataToSave[`expertsPerProject/${projectId}/${newExpertKey}`] = true;

    return this.firebaseUpdate(dataToSave);

  }

  editExpert(expertId: string, expert: Expert): Observable<any> {
        expert.edition = new Date();
        const expertToSave = Object.assign({}, expert);
        delete(expertToSave.$key);

        let dataToSave = {};
        dataToSave[`experts/${expertId}`] = expertToSave;

        return this.firebaseUpdate(dataToSave);
  }

  disableExpert(expertId: string, expert: Expert): Observable<any>{
    expert.enabled = false;
    expert.edition = new Date();
    const expertToSave = Object.assign({}, expert);
    delete(expertToSave.$key);

    let dataToSave = {};
    dataToSave[`experts/${expertId}`] = expertToSave;
    dataToSave[`expertsPerProject/${expert.projectId}/${expertId}`] = false;

    return this.firebaseUpdate(dataToSave);
  }

}
/*
  deleteExpert(expertId: string, projectId: string){

      const expertsRef = this.sdkDb.child("experts");
      const expertsPerProjectRef = this.sdkDb.child("expertsPerProject");

      const deleteExpertPromise = expertsRef.child(expertId).remove();

      const deleteExpertPerProjectPromise =
          expertsPerProjectRef.child(`${projectId}/${expertId}`).remove();

      Promise.all([deleteExpertPromise, deleteExpertPerProjectPromise])
          .then(
              () => {
                  console.log("expert deleted");
              }
          )
          .catch(() => {
          console.log("expert deletion in error");
      });

  }
*/
  // requests for batch server by queque
  // requestExpertDeletion(expertId:string, projectId:string) {
  //     this.sdkDb.child('queue/tasks').push({expertId,projectId})
  //         .then(
  //             () => alert('expert deletion requested !')
  //         );
  // }