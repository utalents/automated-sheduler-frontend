import { Injectable , Inject} from '@angular/core';
import {Http} from "@angular/http";

import {AngularFireDatabase, FirebaseRef} from "angularfire2";
import {FirebaseListFactoryOpts} from "angularfire2/interfaces";
import {firebaseConfig} from "../../../environments/firebase.config";

import {Observable, Subject} from "rxjs/Rx";

import {DataService} from "./data.service";

import {Project} from "../model/project";

@Injectable()
export class ProjectService extends DataService{

  constructor(private db:AngularFireDatabase, @Inject(FirebaseRef) fb, private http: Http) {
      super(db, fb, http);               
  }

  findAllProjects():Observable<Project[]>{
      return this.db.list('projects', {
        query: {
          orderByChild: 'enabled',
          equalTo: true
        }
      }).map(Project.fromJsonArray);
  }

  findProjectsQuequeCount(projectId: string): Observable<any>{
    return this.db.list('mailQuequesPerProject/'+ projectId,{
      query: {
        orderByValue: true,
        equalTo: true
      }
    })
  }

  findProjectsExpertsCount(projectId: string): Observable<any>{
    return this.db.list('expertsPerProject/'+ projectId,{
      query: {
        orderByValue: true,
        equalTo: true
      }
    })
  }

  findProjectsByState(stateId: string):Observable<Project[]>{
      return this.db.list('projects', {
        query: {
          orderByChild: 'stateId',
          equalTo: stateId
        }
      }).map(Project.fromJsonArray);
  }

  createNewProject(project: any): Observable<any>{
    const projectToSave = Object.assign({}, project, {enabled: true, creation: new Date(), edition: null});
    const newProjectKey = this.sdkDb.child('projects').push().key;
    let dataToSave = {};

    dataToSave["projects/" + newProjectKey] = projectToSave;

    return this.firebaseUpdate(dataToSave);
  }

  editProject(projectId: string, project: Project): Observable<any>{
    project.edition = new Date();
    const projectToSave = Object.assign({}, project);
    delete(projectToSave.$key);

    let dataToSave = {};
    dataToSave[`projects/${projectId}`] = projectToSave;

    return this.firebaseUpdate(dataToSave);
  
  }
  
  disableProject(projectId: string, project: Project): Observable<any>{
    project.enabled = false;
    project.stateId = '2';
    project.edition = new Date();
    const projectToSave = Object.assign({}, project);
    delete(projectToSave.$key);

    let dataToSave = {};
    dataToSave[`projects/${projectId}`] = projectToSave;

    return this.firebaseUpdate(dataToSave);
  }



}
