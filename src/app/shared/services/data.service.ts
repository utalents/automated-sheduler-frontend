import { Injectable, Inject } from '@angular/core';

import {Http} from "@angular/http";

import {AngularFireDatabase, FirebaseRef} from "angularfire2";
import {FirebaseListFactoryOpts} from "angularfire2/interfaces";
import {firebaseConfig} from "../../../environments/firebase.config";

import {Observable, Subject} from "rxjs/Rx";

@Injectable()
export class DataService {

  sdkDb:any;

  constructor(db:AngularFireDatabase, @Inject(FirebaseRef) fb, http:Http) {
    this.sdkDb = fb.database().ref();
  }

  firebaseUpdate(dataToSave) {
    
        const subject = new Subject();

        this.sdkDb.update(dataToSave)
            .then(
                val => {
                    subject.next(val);
                    subject.complete();

                },
                err => {
                    subject.error(err);
                    subject.complete();
                }
            );

        return subject.asObservable();
    }
}
