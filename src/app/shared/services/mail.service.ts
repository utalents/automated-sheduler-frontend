import { Injectable , Inject} from '@angular/core';
import {Http} from "@angular/http";

import {AngularFireDatabase, FirebaseRef} from "angularfire2";
import {FirebaseListFactoryOpts} from "angularfire2/interfaces";
import {firebaseConfig} from "../../../environments/firebase.config";

import {Observable, Subject} from "rxjs/Rx";

import {DataService} from "./data.service";

import {MailTask} from "../model/mailTask";

@Injectable()
export class MailService extends DataService {
  
  constructor(private db:AngularFireDatabase, @Inject(FirebaseRef) fb, private http: Http) {
      super(db, fb, http);               
  }

  findAllQueques():Observable<MailTask[]>{
      return this.db.list('mailQueques', {
        query: {
          orderByChild: 'enabled',
          equalTo: true
        }
      }).map(MailTask.fromJsonArray);
  }
  
  findQuequesByProject(projectId: string): Observable<MailTask[]>{
      return this.db.list('mailQueques', {
        query: {
        orderByChild: 'projectId',
        equalTo: projectId,
        }
      })
      .map(queques => queques.filter(queque => queque.enabled))
      .map(MailTask.fromJsonArray);
  }


  createNewQueque(projectId: string, mailTask: any): Observable<any>{
    let defautValues = {enabled: true, creation: new Date(), edition: null, sendDate : null, generateDate : null};
    const mailToSave = Object.assign({}, mailTask, {projectId}, defautValues);
    const newQuequeKey = this.sdkDb.child('mailQueques').push().key;
    let dataToSave = {};

    dataToSave["mailQueques/" + newQuequeKey] = mailToSave;
    dataToSave[`mailQuequesPerProject/${projectId}/${newQuequeKey}`] = true;
    dataToSave[`mailQuequesPerExpert/${mailTask.expertId}/${newQuequeKey}`] = true;

    return this.firebaseUpdate(dataToSave);
  }

  editQueque(quequeId: string, mailTask: MailTask): Observable<any>{
    mailTask.edition = new Date();
    mailTask.sendDate = mailTask.sendDate ? mailTask.sendDate : null
    mailTask.generateDate = mailTask.generateDate ? mailTask.generateDate : null;
    const mailToSave = Object.assign({}, mailTask);
    delete(mailToSave.$key);

    let dataToSave = {};
    dataToSave[`mailQueques/${quequeId}`] = mailToSave;

    return this.firebaseUpdate(dataToSave);
  
  }
  disableQueque(quequeId: string, mailTask: MailTask): Observable<any>{
    mailTask.enabled = false;
    mailTask.stateId = '2';
    mailTask.edition = new Date();
    mailTask.sendDate = mailTask.sendDate ? mailTask.sendDate : null
    mailTask.generateDate = mailTask.generateDate ? mailTask.generateDate : null;
    const mailToSave = Object.assign({}, mailTask);
    delete(mailToSave.$key);

    let dataToSave = {};
    dataToSave[`mailQueques/${quequeId}`] = mailToSave;
    dataToSave[`mailQuequesPerProject/${mailTask.projectId}/${quequeId}`] = false;
    dataToSave[`mailQuequesPerExpert/${mailTask.expertId}/${quequeId}`] = false;

    return this.firebaseUpdate(dataToSave);
  }

}
