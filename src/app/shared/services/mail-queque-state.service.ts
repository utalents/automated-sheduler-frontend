import { Injectable, Inject } from '@angular/core';
import { AngularFireDatabase, FirebaseRef } from "angularfire2";
import { Http } from "@angular/http";

import { DataService } from "app/shared/services/data.service";

import {Observable, Subject} from "rxjs/Rx";
import { MailQuequeState } from "app/shared/model/mailQuequeState";

@Injectable()
export class MailQuequeStateService  extends DataService {
  
  constructor(private db:AngularFireDatabase, @Inject(FirebaseRef) fb, private http: Http) {
      super(db, fb, http);               
  }

  findAllStates() : Observable<MailQuequeState[]>{
      return this.db.list('mailQuequeStates', {}).map(MailQuequeState.fromJsonArray);
  }

}
