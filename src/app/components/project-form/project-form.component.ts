import { Component, Input, Output, SimpleChanges, OnInit, OnChanges, EventEmitter } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from "@angular/forms";
import { ProjectService } from "app/shared/services/project.service";
import { Project } from "app/shared/model/project";

@Component({
  selector: 'project-form',
  templateUrl: './project-form.component.html',
  styleUrls: ['./project-form.component.css'],
  providers: [ProjectService]
})
export class ProjectFormComponent implements OnInit, OnChanges  {

  projectform:FormGroup;
  states : [{$key: string, value: string}];

  @Input()
  project: Project;

  @Input()
  modalOpen: boolean;

  @Output() notify: EventEmitter<string> = new EventEmitter<string>();

  currentProject : Project;

  constructor(private fb:FormBuilder, private projectService: ProjectService) 
  { this.createForm();}

  ngOnInit() {
    //TODO change to state machine.
    this.states = [
      {$key:'1', value:"Активен"}, 
      {$key:'2', value:"Остановлен"}
    ];
    this.projectform.patchValue({stateId: undefined});
  }
  
  ngOnChanges(changes: SimpleChanges) {
    if (changes['project']) {
        this.currentProject = changes['project'].currentValue;
        this.projectform.patchValue(this.currentProject);
    }
  }

  onOpenChanged($event) {
    this.clearData();
  }

  createForm(){
    this.projectform = this.fb.group({
      name: ['',Validators.required],
      appScriptId: ['',Validators.required],
      stateId: ['',Validators.required],
    });
  }

  create(){
    const formValue = this.projectform.value;
    this.projectService.createNewProject(formValue)
        .subscribe(
            () => {
                console.log("Project created succesfully.");
                this.clearData();
            },
            err => alert(`error creating project ${err}`)
        );
  }
  edit(){
    const formValue = this.projectform.value;
    //TODO change to data bindins
    this.currentProject.name = formValue.name;
    this.currentProject.appScriptId = formValue.appScriptId;
    this.currentProject.stateId = formValue.stateId;

    this.projectService.editProject(this.currentProject.$key, this.currentProject)
        .subscribe(
            () => {
                console.log("Project edited succesfully.");
                this.clearData();
            },
            err => alert(`error editing project ${err}`)
        );
    this.modalOpen = false;
  }
  cancel(){
    this.clearData();
  }

  checkValidation(field : string){
    return this.projectform.get(field).invalid && 
          (this.projectform.get(field).dirty || this.projectform.get(field).touched)
  }

  clearData(){
    this.projectform.reset();
    this.notify.emit('clearData');
  }
}
