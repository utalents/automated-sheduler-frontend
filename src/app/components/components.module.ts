import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from 'clarity-angular';
//DEPRICATED
import { ButtonModule }  from 'primeng/primeng';

import { ExpertFormComponent } from "app/components/expert-form/expert-form.component";
import { MailFormComponent } from "app/components/mail-form/mail-form.component";
import { ProjectFormComponent } from "app/components/project-form/project-form.component";
import { ProjectSelectorComponent } from "app/components/project-selector/project-selector.component";

const components = [
  ExpertFormComponent,
  MailFormComponent,
  ProjectFormComponent,
  ProjectSelectorComponent,
];

@NgModule({
  imports: [
    ClarityModule,
    CommonModule,
    ButtonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    ...components,
  ],
  exports: [
    ...components,
  ]
})
export class ComponentsModule { }
