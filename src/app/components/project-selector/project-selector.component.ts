import { Component, Output, SimpleChanges, OnInit, OnChanges, EventEmitter } from '@angular/core';

import {Observable} from "rxjs/Rx";

import { ProjectService } from "app/shared/services/project.service";
import { Project } from "app/shared/model/project";

@Component({
  selector: 'project-selector',
  templateUrl: './project-selector.component.html',
  styleUrls: ['./project-selector.component.css'],
  providers: [ProjectService]
})
export class ProjectSelectorComponent implements OnInit  {

  private projects$: Observable<Project[]>;
  private projectId: string;

  @Output() projectSelected: EventEmitter<string> = new EventEmitter<string>();

  constructor(private projectService: ProjectService) { }

  ngOnInit() {
    this.projectId = undefined;
    //TODO change state number to stateID, or 'Active' synonym
    this.projects$ =  this.projectService.findProjectsByState('1');
  }

  onProjectSelected(event:Event):void {
      this.projectSelected.emit(this.projectId);
  }

}
