import { Component, Input, Output, SimpleChanges, OnInit, OnChanges, EventEmitter } from '@angular/core';
import {Validators, FormGroup, FormBuilder} from "@angular/forms";

import {ExpertsService} from "app/shared/services/experts.service";
import {Expert} from "app/shared/model/expert";
@Component({
  selector: 'expert-form',
  templateUrl: './expert-form.component.html',
  styleUrls: ['./expert-form.component.css'],
  providers: [ExpertsService]
})
export class ExpertFormComponent implements OnInit, OnChanges {

  expertForm:FormGroup;
  
  @Input()
  projectId: string;

  @Input()
  modalOpen: boolean;

  @Input()
  expert:Expert;
  
  @Output() notify: EventEmitter<string> = new EventEmitter<string>();

  currentExpert: Expert;

  constructor(private fb:FormBuilder, private expertsService: ExpertsService) {
      this.createForm();
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['expert']) {
        this.currentExpert = changes['expert'].currentValue;
        this.expertForm.patchValue(this.currentExpert);
    }
  }

  createForm(){
      this.expertForm = this.fb.group({
        email: ['',Validators.required],
        name: ['',Validators.required],
        surname: ['',Validators.required],
        patronymic: ['',Validators.required],
        description: ['']
    });
  }

  create(){
    const formValue = this.expertForm.value;
    this.expertsService.createNewExpert(this.projectId, formValue)
        .subscribe(
            () => {
                console.log("Expert created succesfully.");
                this.clearData();
            },
            err => alert(`error creating expert ${err}`)
        );
  }

  edit(){
    const formValue = this.expertForm.value;
    //TODO change to data bindins
    this.currentExpert.email = formValue.email;
    this.currentExpert.name = formValue.name;
    this.currentExpert.surname = formValue.surname;
    this.currentExpert.patronymic = formValue.patronymic;
    this.currentExpert.description = formValue.description || null;

    this.expertsService.editExpert(this.currentExpert.$key, this.currentExpert)
        .subscribe(
            () => {
                console.log("Expert edited succesfully.");
                this.clearData();
            },
            err => alert(`error editing expert ${err}`)
        );
    this.modalOpen = false;
  }

  cancel(){
      this.clearData();
  }
  
  onOpenChanged($event) {
    this.clearData();
  }

  checkValidation(field : string){
    return this.expertForm.get(field).invalid && 
          (this.expertForm.get(field).dirty || this.expertForm.get(field).touched)
  }

  clearData(){
    this.expertForm.reset();
    this.notify.emit('clearData');
  }

}