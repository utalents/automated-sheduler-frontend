import { Component, Input, SimpleChanges, OnInit, OnChanges, Output, EventEmitter } from '@angular/core';
import {Validators, FormGroup, FormBuilder} from "@angular/forms";

import {MailService} from "app/shared/services/mail.service";
import {ExpertsService} from "app/shared/services/experts.service";
import { MailQuequeStateService } from "app/shared/services/mail-queque-state.service";

import {MailTask} from "app/shared/model/mailTask";
import {Expert} from "app/shared/model/expert";
import { MailQuequeState } from "app/shared/model/mailQuequeState";

import {Observable} from "rxjs/Rx";
import * as _ from "lodash";


@Component({
  selector: 'mail-form',
  templateUrl: './mail-form.component.html',
  styleUrls: ['./mail-form.component.css'],
  providers: [MailService, ExpertsService, MailQuequeStateService]
})
export class MailFormComponent implements OnInit, OnChanges {

  quequeForm: FormGroup;

  @Input()
  projectId: string;

  @Input()
  modalOpen: boolean;

  @Input()
  queque: MailTask;

  @Output() notify: EventEmitter<string> = new EventEmitter<string>();
  
  currentQueque: MailTask;

  states : MailQuequeState[];
  
  experts:  Expert[];

  constructor(private fb:FormBuilder, 
              private mailService: MailService, 
              private expertsService: ExpertsService,
              private mailQuequeStateService: MailQuequeStateService) { 
    this.createForm();
  }

  ngOnInit() {
    this.mailQuequeStateService.findAllStates().subscribe(
        data => {
          this.states = data;
          this.quequeForm.patchValue({stateId: this.states[0].$key, expertId: undefined});
        }
    );
  }

  ngOnChanges(changes:SimpleChanges) {
      if (changes['queque']) {
          this.currentQueque = changes['queque'].currentValue;
          this.quequeForm.patchValue(this.currentQueque);
      }
      if (changes['projectId']) {
        this.expertsService.findExpertByProject(this.projectId).subscribe(
          data => this.experts = data
        );
      }
  }

  createForm(){
    this.quequeForm = this.fb.group({
        requestId: ['',Validators.required],
        expertId: ['',Validators.required],
        stateId: ['']
    });
  }

  create(){
    const formValue = this.quequeForm.value;
    console.log(formValue);
    this.mailService.createNewQueque(this.projectId, formValue)
        .subscribe(
            () => {
                console.log("Mail queque created succesfully.");
                this.clearData();
            },
            err => alert(`error creating queque ${err}`)
        );
  }

  edit(){
    const formValue = this.quequeForm.value;
    this.currentQueque.requestId = formValue.requestId;
    this.currentQueque.expertId = formValue.expertId;
    this.currentQueque.stateId = formValue.stateId;
    this.currentQueque.documentId = this.currentQueque.documentId || null;

    this.mailService.editQueque(this.currentQueque.$key, this.currentQueque)
        .subscribe(
            () => {
                console.log("Mail queque edited succesfully.");
                this.clearData();
            },
            err => alert(`error editing queque ${err}`)
        );
    this.modalOpen = false;
  }
  
  cancel(){
    this.clearData();
  }

  onOpenChanged($event) {
    this.clearData();
  }

  checkValidation(field : string){
    return this.quequeForm.get(field).invalid && 
          (this.quequeForm.get(field).dirty || this.quequeForm.get(field).touched)
  }

  getExpertById($key: string): any
  {
    return this.getById(this.experts, $key);
  }

  getStateById($key: string):any {
    return this.getById(this.states, $key);
  }

  getAvailableStates(){
    let filtered = _.filter(this.states, item => this.filterStates(item));
    return filtered;
  }
  
  private filterStates(item): boolean{
      let currentState = this.getStateById(this.currentQueque.stateId).state;
      return item.state == currentState || item.state == "Canceled"
  }

 // TODO create parent class and extends it of him
  private getById(data: any[], $key: string): any {
    let result = _.find( data, (item: any) => item.$key == $key);
    return result;
  }
  clearData(){
    this.quequeForm.reset();
    this.notify.emit('clearData');
  }

}
