import { ScienceJournalPage } from './app.po';

describe('science-journal App', () => {
  let page: ScienceJournalPage;

  beforeEach(() => {
    page = new ScienceJournalPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
